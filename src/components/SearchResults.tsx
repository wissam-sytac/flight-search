import './SearchResults.css';
import React from 'react';
import { State, Status } from '../service/states';
import Result from './Result';
import { Flight } from '../service/flights';
import Loading from './Loading';
import ApiError from './ApiError';
import Toolbar from './Toolbar';

const ERROR_CONSTS = {
  SERVER_ERROR: 'Wij kunnen de data momentaal niet ophalen',
};

function SearchResults(props: {
  state: State;
  setSort: (...args: any[]) => any;
}) {
  const { state, setSort } = props;

  const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const value = event.target.value;
    setSort(value);
  };

  if (state.status === Status.Loading) {
    return <Loading />;
  }

  if (state.error) {
    return <ApiError message={ERROR_CONSTS.SERVER_ERROR} />;
  }

  if (state.status === Status.Success) {
    const data = state.filteredData as Flight[];

    if (!data.length) {
      return (
        <div aria-live="polite" className="SearchResults">
          <p>no results matching your query</p>
        </div>
      );
    }

    return (
      <div aria-live="polite" className="SearchResults">
        <Toolbar
          sortFunction={handleSelectChange}
          numberOfResults={data.length}
        />
        <ul>
          {data.map((flight) => (
            <Result key={flight.flightIdentifier} flight={flight} />
          ))}
        </ul>
      </div>
    );
  }
  return <div>no data</div>;
}

export default SearchResults;
