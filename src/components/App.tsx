import '../styles/resets.css';
import './App.css';
import React, { useEffect, useState } from 'react';
import SearchBox from './SearchBox';
import SearchResults from './SearchResults';
import { getFlights } from '../service/api';
import { EventTypes, useFlightData } from '../hooks/use-flightdata';
import { SortOrder } from '../service/states';

function App() {
  const [searchKeyword, setSearchKeyword] = useState('');
  const [state, dispatch] = useFlightData(null);
  const [sortBy, setSortBy] = useState<SortOrder>('asc');

  const orderByDate = (order: SortOrder) => {
    setSortBy(order);
    dispatch({ type: EventTypes.SORT_BY_TIME, orderBy: order });
  };

  useEffect(() => {
    (async () => {
      dispatch({ type: EventTypes.FETCH });
      try {
        const response = await getFlights();
        dispatch({
          type: EventTypes.RESOLVE,
          data: response.data.flights,
          orderBy: sortBy,
        });
      } catch (err) {
        // do something
      }
    })();
  }, [dispatch]);

  useEffect(() => {
    const filterByKeyword = (k: string) => {
      return dispatch({ type: EventTypes.SEARCH, payload: k });
    };
    filterByKeyword(searchKeyword);
  }, [searchKeyword, dispatch]);

  return (
    <div className="App-container">
      <div className="App">
        <div className="App-header">
          <h1>Flight search</h1>
          <SearchBox
            setSearchKeyword={setSearchKeyword}
            debounceDuration={1500}
            minCharactersForSearch={3}
          />
        </div>
        <div className="App-results">
          <SearchResults state={state} setSort={orderByDate} />
        </div>
      </div>
    </div>
  );
}

export default App;
