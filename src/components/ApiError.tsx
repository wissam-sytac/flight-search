function ApiError(props: { message: string }) {
  return (
    <div role="alert" className="InputError">
      {props.message}
    </div>
  );
}

export default ApiError;
