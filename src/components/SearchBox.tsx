import React, { useCallback, useMemo, useState } from 'react';
import './SearchBox.css';
import _ from 'lodash';
import { Nullable } from '../service/states';
import InputError from './InputError';

function SearchBox(props: {
  setSearchKeyword: (...args: any[]) => any;
  debounceDuration: number;
  minCharactersForSearch: number;
}) {
  const { setSearchKeyword, debounceDuration, minCharactersForSearch } = props;
  const [error, setError] = useState<Nullable<string>>('');

  const handleChange = useCallback(
    (evt: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = evt.target;
      if (value && value.length < minCharactersForSearch) {
        return setError(
          `U dient meer karakters in te typen (min ${minCharactersForSearch})`
        );
      }
      setError(null);
      setSearchKeyword(value);
    },
    [setSearchKeyword, minCharactersForSearch]
  );

  const debouncedChangeHandler = useMemo(
    () => _.debounce(handleChange, debounceDuration),
    [handleChange, debounceDuration]
  );

  return (
    <form id="site-search" role="search" className="SearchBar">
      <input
        type="search"
        id="site-search-input"
        placeholder="Start typing airport destination"
        spellCheck="false"
        onChange={debouncedChangeHandler}
        className="SearchBar-input"
        aria-invalid={!!error}
      />
      {error && <InputError message={error} />}
    </form>
  );
}

export default SearchBox;
