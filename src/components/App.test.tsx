import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { getFlights } from '../service/api';

jest.mock('../service/api', () => ({
  getFlights: jest.fn(),
}));

describe('App', () => {
  test('Displays heading correctly', async () => {
    render(<App />);
    expect(await screen.findByText(/Flight search/i)).toBeInTheDocument();
  });

  test('Displays loading message', async () => {
    render(<App />);
    expect(await screen.findByText(/loading/i)).toBeInTheDocument();
  });

  test('Displays flight results', async () => {
    (getFlights as jest.Mock).mockImplementation(() => {
      return Promise.resolve({
        data: {
          flights: [
            {
              flightIdentifier: 'D20190401UA969',
              flightNumber: 'UA 969',
              airport: 'San Francisco',
              expectedTime: '14:50',
              originalTime: '14:50',
              url: '/en/departures/flight/D20190401UA969/',
              score: '70.55272',
            },
          ],
        },
      });
    });
    render(<App />);
    expect(await screen.findByText(/Results: 1/i)).toBeInTheDocument();
  });

  test('Displays 0 results', async () => {
    (getFlights as jest.Mock).mockImplementation(() => {
      return Promise.resolve({
        data: {
          flights: [],
        },
      });
    });
    render(<App />);
    expect(await screen.findByText(/no results/i)).toBeInTheDocument();
  });
});
