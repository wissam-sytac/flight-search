import './Toolbar.css';
import React from 'react';

function Toolbar(props: {
  sortFunction: (...args: any[]) => any;
  numberOfResults: number;
}) {
  const { sortFunction, numberOfResults } = props;
  return (
    <div className="SearchResults-toolbar">
      <div className="SearchResults-total">Results: {numberOfResults}</div>
      <div>
        <select onChange={sortFunction}>
          <option value="asc">Expected Time (Asc)</option>
          <option value="desc">Expected Time (Desc)</option>
        </select>
      </div>
    </div>
  );
}

export default Toolbar;
