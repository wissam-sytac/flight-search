import './Result.css';
import { Flight } from '../service/flights';

function Result(props: { flight: Flight }) {
  const {
    flight: { expectedTime, originalTime, airport, flightNumber, url },
  } = props;

  const isFlightOnTime = expectedTime.isSame(originalTime);

  return (
    <li className="Result">
      <div className="Result-flightinfo">
        <div className="Result-flightinfo-time">
          <div className="Result-flightinfo-time-expected">
            {expectedTime.format('hh:mm')}
          </div>
          {!isFlightOnTime && (
            <div className="Result-flightinfo-time-original">
              {originalTime.format('hh:mm')}
            </div>
          )}
        </div>
        <div className="Result-flightinfo-destination">
          <div>{airport}</div>
          <div>{flightNumber}</div>
        </div>
        <div>
          <a className="Result-btnDetails" href={url}>
            go to details
          </a>
        </div>
      </div>
    </li>
  );
}

export default Result;
