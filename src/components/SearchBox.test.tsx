import React from 'react';
import { render, screen } from '@testing-library/react';
import SearchBox from './SearchBox';
import userEvent from '@testing-library/user-event';

const setup = () => {
  const utils = render(
    <SearchBox
      setSearchKeyword={(a) => a}
      debounceDuration={200}
      minCharactersForSearch={3}
    />
  );
  const input = utils.getByPlaceholderText(
    /Start typing airport destination/i
  ) as HTMLInputElement;
  return {
    input,
    ...utils,
  };
};

describe('SearchBox', () => {
  test('renders the input', () => {
    render(
      <SearchBox
        setSearchKeyword={(a) => a}
        debounceDuration={1000}
        minCharactersForSearch={3}
      />
    );
    expect(
      screen.getByPlaceholderText(/Start typing airport destination/i)
    ).toBeInTheDocument();
  });

  test('shows error when typing less than 3 characters', async () => {
    const { input } = setup();
    await userEvent.type(input, 'aa');
    expect(
      await screen.findByText(/U dient meer karakters/i)
    ).toBeInTheDocument();
  });
});
