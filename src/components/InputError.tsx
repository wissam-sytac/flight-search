import './InputError.css';

function InputError(props: { message: string }) {
  return (
    <div role="alert" className="InputError">
      {props.message}
    </div>
  );
}

export default InputError;
