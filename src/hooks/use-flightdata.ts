import { useReducer } from 'react';
import {
  IdleState,
  Nullable,
  SortOrder,
  State,
  Status,
} from '../service/states';
import {
  filterFlightsByAirportName,
  flightFromApiResponse,
  FlightRecord,
  Flights,
  sortFlightsByExpectedTime,
} from '../service/flights';

export enum EventTypes {
  FETCH,
  RESOLVE,
  REJECT,
  SEARCH,
  SORT_BY_TIME,
}

type Event =
  | { type: EventTypes.FETCH }
  | { type: EventTypes.RESOLVE; data: FlightRecord[]; orderBy: SortOrder }
  | { type: EventTypes.REJECT; error: Error }
  | { type: EventTypes.SEARCH; payload: string }
  | { type: EventTypes.SORT_BY_TIME; orderBy: SortOrder };

function useFlightData(initialData: Nullable<Flights> = null) {
  const initialState: IdleState = {
    status: Status.Idle,
    data: initialData,
    error: null,
  };

  function fetchReducer(state: State, event: Event): State {
    switch (event.type) {
      case EventTypes.FETCH:
        return {
          ...state,
          status: Status.Loading,
        };
      case EventTypes.RESOLVE:
        const { data, orderBy } = event;
        const transformedDataAndSorted = sortFlightsByExpectedTime(
          data.map(flightFromApiResponse),
          orderBy
        );
        return {
          status: Status.Success,
          data: transformedDataAndSorted,
          filteredData: transformedDataAndSorted,
          error: null,
        };
      case EventTypes.REJECT:
        return {
          status: Status.Failure,
          data: null,
          error: event.error,
        };
      case EventTypes.SEARCH:
        if (state.status === Status.Success) {
          return {
            ...state,
            filteredData: filterFlightsByAirportName(event.payload, state.data),
          };
        }
        return state;
      case EventTypes.SORT_BY_TIME:
        if (state.status === Status.Success) {
          const filteredData = [...state.filteredData];
          return {
            ...state,
            filteredData: sortFlightsByExpectedTime(
              filteredData,
              event.orderBy
            ),
          };
        }
        return state;
      default:
        return state;
    }
  }

  return useReducer(fetchReducer, initialState);
}

export { useFlightData };
