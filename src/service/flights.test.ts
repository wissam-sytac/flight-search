import {
  filterFlightsByAirportName,
  sortFlightsByExpectedTime,
} from './flights';
import { flightFixtures } from './fixtures';
import { SortOrder } from './states';

describe('flights', () => {
  describe('filterFlightsByAirportName', () => {
    test('works for empty keyword', () => {
      const poging = filterFlightsByAirportName('', [
        flightFixtures['1'],
        flightFixtures['2'],
      ]);
      const verwacht = [flightFixtures['1'], flightFixtures['2']];
      expect(poging.sort()).toEqual(verwacht.sort());
    });
    test('searches correctly on sequence of characters', () => {
      const poging1 = filterFlightsByAirportName('san', [
        flightFixtures['1'],
        flightFixtures['2'],
        flightFixtures['3'],
      ]);
      const verwacht1 = [
        flightFixtures['1'],
        flightFixtures['2'],
        flightFixtures['3'],
      ];
      expect(poging1.sort()).toEqual(verwacht1.sort());

      const poging2 = filterFlightsByAirportName('lond', [
        flightFixtures['5'],
        flightFixtures['2'],
        flightFixtures['3'],
      ]);
      const verwacht2 = [flightFixtures['5']];
      expect(poging2.sort()).toEqual(verwacht2.sort());
    });
    test('works correctly for different lower/upper case combinations', () => {
      const poging2 = filterFlightsByAirportName('OnDoN', [
        flightFixtures['5'],
        flightFixtures['2'],
      ]);
      const verwacht2 = [flightFixtures['5']];
      expect(poging2.sort()).toEqual(verwacht2.sort());
    });
  });

  describe('sortFlightsByExpectedTime', () => {
    test('sorts correctly asc', () => {
      const poging = sortFlightsByExpectedTime(
        [flightFixtures['2'], flightFixtures['5'], flightFixtures['4']],
        'asc'
      );
      const verwacht = [
        flightFixtures['5'],
        flightFixtures['2'],
        flightFixtures['4'],
      ];
      expect(poging).toEqual(verwacht);
    });
    test('sorts correctly desc', () => {
      const poging = sortFlightsByExpectedTime(
        [flightFixtures['5'], flightFixtures['2'], flightFixtures['4']],
        'desc'
      );
      const verwacht = [
        flightFixtures['4'],
        flightFixtures['2'],
        flightFixtures['5'],
      ];
      expect(poging).toEqual(verwacht);
    });
  });
});
