import moment, { Moment } from 'moment/moment';
import { SortOrder } from './states';

interface FlightRecord {
  flightIdentifier: string;
  flightNumber: string;
  airport: string;
  expectedTime: string;
  originalTime: string;
  url: string;
  score: string;
}

interface Flight {
  flightIdentifier: string;
  flightNumber: string;
  airport: string;
  expectedTime: Moment;
  originalTime: Moment;
  url: string;
  score: Number;
}

export function filterFlightsByAirportName(
  searchKeyword: string,
  flights: Flight[]
): Flight[] {
  if (!searchKeyword) {
    return flights;
  }
  return flights.filter((f) =>
    f.airport.toLowerCase().includes(searchKeyword.toLowerCase())
  );
}

function compareFlightsByExpectedTime(
  f1: Flight,
  f2: Flight,
  order: SortOrder = 'asc'
): number {
  if (f1.expectedTime.isSame(f2.expectedTime)) {
    return 0;
  }
  if (order === 'desc') {
    return f1.expectedTime.isBefore(f2.expectedTime) ? 1 : -1;
  }
  return f1.expectedTime.isBefore(f2.expectedTime) ? -1 : 1;
}

export function sortFlightsByExpectedTime(
  flights: Flight[],
  sortOrder: SortOrder
) {
  return flights.sort((f1, f2) => {
    return compareFlightsByExpectedTime(f1, f2, sortOrder);
  });
}

export function flightFromApiResponse(f: FlightRecord): Flight {
  return {
    flightIdentifier: f.flightIdentifier,
    flightNumber: f.flightNumber,
    airport: f.airport,
    expectedTime: moment(f.expectedTime, 'hh:mm'),
    originalTime: moment(f.originalTime, 'hh:mm'),
    url: f.url,
    score: Number(f.score),
  };
}

type Flights = Flight[];

export type { Flights };
export type { FlightRecord };
export type { Flight };
