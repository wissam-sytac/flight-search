import { Flights } from './flights';

export type SortOrder = 'asc' | 'desc';

export enum Status {
  Idle,
  Loading,
  Success,
  Failure,
}

interface Error {
  message: string;
}

export type { Error };

type Nullable<T> = T | null | undefined;

export type IdleState = {
  status: Status.Idle;
  data: Nullable<Flights>;
  error: null;
};
type LoadingState = {
  status: Status.Loading;
  data: Nullable<Flights>;
  error: Nullable<Error>;
};

type SuccessState = {
  status: Status.Success;
  data: Flights;
  filteredData: Flights;
  error: null;
};

type FailureState = {
  status: Status.Failure;
  data: null;
  error: Error;
};

type State = IdleState | LoadingState | SuccessState | FailureState;

export type { State };
export type { SuccessState };
export type { LoadingState };
export type { Nullable };
