// api.js
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import flights from '../seed-data/flights.json';

const axiosInstance = axios.create({
  baseURL: process.env.FLIGHTS_API_BASE_URL || '',
});

export const mock = new MockAdapter(axiosInstance);

if (process.env.RUN_LIVE_FLIGHTS_API !== 'true') {
  mock.onGet('/flights').reply(200, flights);
}

const getFlights = async (): Promise<any> =>
  await axiosInstance.get('/flights');

export { axiosInstance, getFlights };
