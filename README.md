# Flight Search Web App

## Assumptions and Shortcuts
* The remote flights api is being mocked using a Axios mock (see `src/services/api.ts` L:13)
* Error scenarios from the api calls not properly tested (due to time constraints)
* Low test coverage (due to time constraints)
* The search box does not ignore spaces (does not trim the input)
* Clear input functionality not properly implemented
* Used Moment package for date manipulation
* There are security vulnerabilities in the peer dependencies (due to time constraints)
* There are some react warnings left (no time to investigate)

## Tech stack

This project was bootstrapped with Create React App and uses **Typescript**.

## Available Scripts
In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.


